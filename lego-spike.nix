{ config, pkgs, ... }:
{
    services.udev.packages = [
        (pkgs.writeTextFile {
            name = "50-myusb.rules";
            text = ''
                KERNEL=="ttyACM0",MODE="0666"
            '';
            destination = "/etc/udev/rules.d/50-myusb.rules";
        })
    ];
    environment.systemPackages = with pkgs; [ screen ];
}